#!/usr/bin/env python
from __future__ import print_function
import shutil, os
import csv
 

def createFolder(directory):
    try:
        if not os.path.exists(directory):
            os.makedirs(directory)
    except OSError:
        print ('Error creando directorio. ' +  directory)

def copyFile(src, dest):
    try:
        shutil.copy(src, dest)
    except shutil.Error as e:
        print('Error: %s' % e)
    except IOError as e:
        print('Error: %s' % e.strerror)

def main(): 
    print('Hola, antes de continuar, asegurese de tener una carpeta con las fotos a parte de los archivos csv y demases, ademas, tener este script en la misma carpeta en que se encuentra la planilla.\n')

    print ('1-7. Redacte la ruta en que se encuentra la carpeta de imagenes.')
    path = input() #Por definir la ruta en que se leerán las fotos, importante no sea en la misma carpeta

    print ('2-7. ¿Cómo se llama la planilla csv? (No aguanta excel)')
    excelcsv = input()

    print('\nLas columnas comienzan en 0, asi: A=0, B=1, etc')

    print ('3-7. ¿En qué columna de la planilla se encuentran los SKU?')
    columnaSKU = int(input())

    print ('4-7. ¿En qué columna de la planilla se encuentran las imágenes?')
    columnaImagenes = int(input())

    print ('\n5-7. ¿Cuál es el delimitador por columnas?')
    delimiterCSV = input()

    print ('6-7. ¿Cuál es el delimitador entre imágenes?')
    delimiterImg = input()

    print ('7-7. ¿Cuál es el peso máximo de una imagen en bytes?')
    pesoMaximo = int(input())

    # Definición de variables
    archivospc = os.listdir(path + '//') #Enlista los archivos de la ruta path
    diccionario = {}

    # Abre la edición de los archivos csv
    sinarchivocsv = open('sinarchivo-' + excelcsv + '.csv', 'a')
    sinsku = open('sinsku-' + excelcsv + '.csv','a')
    sobrepeso = open('sobrepeso-' + excelcsv + '.csv','a')
    planillalimpia = open('skueimagen_final-' + excelcsv + '.csv','x')
    imagenescsv = []

    # Escribe la cabecera de los csv
    sobrepeso.write('SKU' + delimiterCSV + 'Imagen' + delimiterCSV + 'Peso Bytes' + '\n')
    planillalimpia.write('SKU' + delimiterCSV + 'Imagen' + '\n')

    # Crea las nuevas carpetas donde copiar las fotos
    createFolder('./Resultados/skueimagen_final/')
    createFolder('./Resultados/sinsku/')
    createFolder('./Resultados/sobrepeso/')

    # Recorre la planilla
    with open(excelcsv + '.csv','r') as archivocsv:
        csv_reader = csv.reader(archivocsv, delimiter=delimiterCSV)
        for fila in csv_reader:
            imagenes = fila[columnaImagenes].split(delimiterImg)
            for imagen in imagenes:
                # Si el nombre de la imagen no se encuentra en la carpeta, se agrega a sinarchivo.csv
                if imagen not in archivospc:
                    sinarchivocsv.write(fila[columnaSKU]+ delimiterCSV + imagen +'\n')
                # Sino, debe guardarse en un diccionario cuya llave única será la imagen
                # para posteriormente confirmar el peso de la imagen
                else:
                    diccionario[imagen] = fila[columnaSKU]
                imagenescsv.append(imagen)

    # Recorre la carpeta de archivos
    for nombre_archivo in archivospc:
        # Si el nombre del archivo no se encuentra en la lista de imagenes de la planilla, se agrega a sinsku.csv
        if nombre_archivo not in imagenescsv:
            sinsku.write(nombre_archivo+'\n')
            copyFile(path + '/' + nombre_archivo, 'Resultados/sinsku/')
        # Sino, significa que el sku e imagen concuerdan y deben filtrarse entre sobrepeso y listos para subir
        else:            
            datosimagen = os.stat(path + '/' + nombre_archivo)
            pesoimagen = datosimagen.st_size
            if pesoimagen > pesoMaximo:
                sobrepeso.write(diccionario[nombre_archivo]+ delimiterCSV + nombre_archivo + delimiterCSV + str(pesoimagen) + '\n')
                copyFile(path + '/' + nombre_archivo, 'Resultados/sobrepeso/')
            else:
                planillalimpia.write(diccionario[nombre_archivo]+ delimiterCSV + nombre_archivo +'\n')
                copyFile(path + '/' + nombre_archivo, 'Resultados/skueimagen_final/')

    # Cierra ficheros
    sobrepeso.close
    planillalimpia.close
    sinsku.close
    sinarchivocsv.close

    print('\nScript terminado, puede revisar los resultados en la misma carpeta donde se encuentra la planilla.\nFin programa')

if __name__ == '__main__': 
	main() 
