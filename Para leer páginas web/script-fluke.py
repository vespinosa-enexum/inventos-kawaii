from lxml import html
import requests
import csv

page = requests.get('https://www.fluke.com/es-ar/producto/captura-de-imagenes-termograficas/termografia/serie-performance/fluke-tis75')
tree = html.fromstring(page.content)

#Lista de ids
overview = tree.xpath('//div[@id="overview-tab"]/section/h2/text()') + tree.xpath('//div[@id="overview-tab"]/section/div/div/div')
#Lista de titulos:
specs = tree.xpath('//div[@id="specs-tab"]/text()')
#Lista de precios
models = tree.xpath('//div[@id="models-tab"]/text()')

print ('General: ', overview, '\n')
print ('Especificaciones: ', specs, '\n')
print ('Modelos: ', models, '\n')


#Estructuraqla
description = """<ul class="nav nav-tabs" role="tablist">
<li role="presentation" class="active"><a href="#1" aria-controls="1" role="tab" data-toggle="tab">Descripción General</a></li>
<li role="presentation"><a href="#2" aria-controls="2" role="tab" data-toggle="tab">Especificaciones</a></li>
<li role="presentation"><a href="#3" aria-controls="3" role="tab" data-toggle="tab">Modelos</a></li>
</ul>
<div class="tab-content">
<div role="tabpanel" class="tab-pane active" id="1">

</div>
<div role="tabpanel" class="tab-pane" id="2">

</div>
<div role="tabpanel" class="tab-pane" id="3">

</div>
</div>"""

#Redacción de un csv con los datos para construir de planilla
f = open('description.csv','w')
f.write(description)
f.close()
