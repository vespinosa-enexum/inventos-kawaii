from lxml import html
import requests
import csv

page = requests.get('http://www.windmade.cl/2-inicio')
tree = html.fromstring(page.content)

#Lista de ids
id_product = tree.xpath('//article/@data-id-product')
#Lista de titulos:
titles = tree.xpath('//h1[@itemprop="name"]/a/text()')
#Lista de precios
prices = tree.xpath('//span[@itemprop="price"]/text()')

print ('ID: ', id_product, '\n')
print ('Titulo: ', titles, '\n')
print ('Precio: ', prices, '\n')

# Pasos para iterar ambos arreglos
result = zip(id_product, titles, prices)
resultSet = set(result)
print('Resultado de la iteración: \n', resultSet)

#Redacción de un csv con los datos para construir de planilla
with open('productos_windmade.csv', mode='w') as productos_windmade:
    windmade_writer = csv.writer(productos_windmade, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    for result in resultSet:
        windmade_writer.writerow(result)
