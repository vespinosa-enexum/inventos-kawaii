#!/usr/bin/env python
from __future__ import print_function
import shutil, os, sys
from PIL import Image

def createFolder(directory):
    try:
        if not os.path.exists(directory):
            os.makedirs(directory)
    except OSError:
        print ('Error creando directorio. ' +  directory)

def intInput():
    try:
        integer = int(input())
        return int(integer)
    except ValueError:
        print ('Error: Debe ser un valor entero.')
        intInput()

def openImage(route_image):
    try:  
        img  = Image.open(route_image)
        return img
    except IOError: 
        print ('Error al abrir: ' + route_image)

def saveImage(route_image, nombre_archivo):
    try:
        route_image.save('Resultados-Birme-Alpha/' + nombre_archivo[0:-4] + '.jpg', 'JPEG', quality=100)
    except AttributeError:
        print("No se puede almacenar imágen de tipo {}".format(route_image))

def pasteBackground(route_image, background, ubication):
    try:
        if route_image.mode == "RGBA" or route_image.mode == "L":
            return background.paste(route_image, ubication, mask=route_image.split()[3])
        else:
            return background.paste(route_image, ubication)
    except AttributeError:
        print("No se puede trabajar con imágen de características {}".format(route_image))

def tupleInput():
    values = input()
    list = values.split(',')
    return tuple(list)

def main(): 
    print('Hola, antes de continuar, asegurese de tener una carpeta con las fotos a parte de los archivos y demases, ademas, tener este script un nivel mas arriba de la carpeta.\n')

    print ('Redacte la ruta en que se encuentra la carpeta de imagenes.')
    path = input() #Por definir la ruta en que se leerán las fotos, importante no sea en la misma carpeta

    print ('Ancho máximo de las imágenes: ')
    ancho_max = intInput()

    print ('Alto máximo de las imágenes: ')
    alto_max = intInput()
    

    # Definición de variables
    archivospc = os.listdir(path + '//') #Enlista los archivos de la ruta path
    size = (ancho_max,alto_max) #Tamaño máximo del ancho o del alto.

    # Crea las nuevas carpetas donde copiar las fotos
    createFolder('./Resultados-Birme-Alpha/')

    # Recorre la carpeta de archivos
    print ('Procesando ...')
    
    for nombre_archivo in archivospc:
        pil_imagen = openImage(path + '/' + nombre_archivo)
        pil_imagen_ancho, pil_imagen_alto = pil_imagen.size
        pil_imagen.load()

        # Si la imagen sobrepasa el alto o ancho estipulado, se redimensiona
        if pil_imagen_ancho > ancho_max or pil_imagen_alto > alto_max:
            pil_imagen.thumbnail(size, Image.ANTIALIAS)
            pil_imagen_ancho, pil_imagen_alto = pil_imagen.size

        # Para controlar avance
        print ("Nombre: " + nombre_archivo + " - Formato: " + pil_imagen.format + " - Lo que esta tras: " + pil_imagen.mode )

        #Modo Sin Transparencia - RGB
        if pil_imagen.mode == "RGB":
            RGB_imagen = pil_imagen.convert('RGB')
            saveImage(RGB_imagen, nombre_archivo)
        
        #Modo Con Transparencia - RGBA, L o P
        elif pil_imagen.mode == "RGBA" or pil_imagen.mode == "L" or pil_imagen.mode == "P":
            # Creación de un fondo cuadrado no mas grande que la dimensión mayor de la imagen
            if (pil_imagen_ancho > pil_imagen_alto) and pil_imagen_ancho < 1000:
                fondo = Image.new("RGB", (pil_imagen_ancho,pil_imagen_ancho), (255, 255, 255))
            elif (pil_imagen_alto > pil_imagen_ancho) and pil_imagen_alto < 1000:
                fondo = Image.new("RGB", (pil_imagen_alto,pil_imagen_alto), (255, 255, 255))
            elif (pil_imagen_alto == pil_imagen_ancho) and pil_imagen_alto < 1000:
                fondo = Image.new("RGB", (pil_imagen_ancho,pil_imagen_alto), (255, 255, 255))
            else:
                fondo = Image.new("RGB", size, (255, 255, 255))
            
            # Centrado de la imagen en caso que su fondo tenga dimensiones mayores
            if pil_imagen_ancho < ancho_max or pil_imagen_alto < alto_max:
                fondo_ancho, fondo_alto = fondo.size
                ubicacion = (((fondo_ancho - pil_imagen_ancho) // 2),((fondo_alto - pil_imagen_alto) // 2))
                pasteBackground(pil_imagen, fondo, ubicacion)
            else:
                ubicacion = (0,0)
                pasteBackground(pil_imagen, fondo, ubicacion)
            saveImage(fondo, nombre_archivo)        
        else:
            print ("Por desarrollar trabajo de tipo {}".format(pil_imagen))

    print ('\nFin del programa. Puede revisar los resultados en la carpeta Resultados.')

if __name__ == '__main__': 
	main() 
