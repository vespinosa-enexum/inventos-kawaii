# Pythono3 code to rename multiple 
# files in a directory or folder 

# importing os module 
import os 

print ('1-1. Redacte la ruta en que se encuentra la carpeta de imagenes.')
path = input() #Por definir la ruta en que se leerán las fotos, importante no sea en la misma carpeta

archivospc = os.listdir(path) #Enlista los archivos de la ruta path

# Function to rename multiple files 
def main(): 
    for filename in os.listdir(path):
        src = path + '/' + filename
        filenamewithout3latestcharacters = filename[0:-4]
        if '.jpg' not in filenamewithout3latestcharacters:
            dst = filename.lower()
        else:
            dst = filename.replace('.jpg', '', 1)
            dst = dst.lower()
        dst = path + '/' + dst

		# rename() function will 
		# rename all the files 
        os.rename(src, dst)

# Driver Code 
if __name__ == '__main__': 
	# Calling main() function 
	main() 
